<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp;

class MainController extends Controller
{
    public function consulta(Request $request){

        $character = $request->get('character');
        $server = $request->get('server');

        return redirect()->route('finder',['server'=> $server, 'username' => $character]);

    }
}
