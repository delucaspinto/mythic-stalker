<?php

namespace App\Http\Controllers;

use GuzzleHttp;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class ApiController extends Controller
{
    public function getBlizzardProfile($server, $username)
    {

        $token = $this->_getToken();

        return Cache::remember('cache-perfil', 1, function () use ($server, $username, $token){

            $server = strtolower($server);
            $username = strtolower($username);

            return [
//                "servers" => $this->getServers($token),
                "personagem" => $this->_getCharacter($username, $server, $token),
                "corredores" => $this->_getCorredores($username, $server),
                "media" => $this->_getMedia($username, $server, $token),
                "miticas" => $this->_getMythic($username, $server, $token),
                "raide" => $this->_getRaide($username, $server, $token)

            ];

        }) ;

    }

    private function _getToken(){

        $client = new GuzzleHttp\Client();

        $res = $client->request('POST', 'https://us.battle.net/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
            ],
            'auth' => [
                env('BLIZZARD_CLIENT_ID'),
                env('BLIZZARD_CLIENT_SECRET')
            ]
        ]);

        if($res->getStatusCode() != 200){
            return [];
        }

        $item = json_decode($res->getBody()->getContents());

        return $item->access_token;

    }

    private function _get($url, $token){

        $client = new GuzzleHttp\Client();

        try {

            $res = $client->request('GET', $url, ['headers' =>
                [
                    'Authorization' => 'Bearer '. $token
                ]
            ]);

        } catch (\Exception $exception){
            return null;
        }

        return $res;

    }

    private function _getCharacter($username, $server, $token){

        $res = $this->_get('https://us.api.blizzard.com/profile/wow/character/'.$server.'/'.$username.'?namespace=profile-us&locale=pt_BR', $token);

        if($res->getStatusCode() != 200){
            return [];
        }

        $res->getHeader('content-type')[0];

        $item = json_decode($res->getBody()->getContents());

        $a = [];
//        $a["id"] = $item->id;
        $a["name"] = $item->name;
        $a["faction"] = $item->faction->name;
        $a["race"] = $item->race->name;
        $a["classe"] = $item->character_class->name;
        $a["activeSpec"] = $item->active_spec->name;
        $a["guild"] = $item->guild->name ?? null;
        $a["level"] = $item->level;
        $a["itemLevel"] = $item->equipped_item_level;
        $a["achievementPoints"] = $item->achievement_points;

        return $a;

    }

    private function _getMedia($username, $server, $token){

        $res = $this->_get('https://us.api.blizzard.com/profile/wow/character/'.$server.'/'.$username.'/character-media?namespace=profile-us', $token);

        $item = json_decode($res->getBody()->getContents());

        $a = $item->assets;

        return $a;

    }

    private function _getMythic($username, $server, $token){

        $res = $this->_get('https://us.api.blizzard.com/profile/wow/character/'.$server.'/'.$username.'/mythic-keystone-profile/season/6?namespace=profile-us&namespace=profile-us&locale=pt_BR', $token);

        if($res == null){
            return [];
        }

        $item = json_decode($res->getBody()->getContents());

        $bests = $item->best_runs;

        return collect($bests)->map(function ($d){

            $g = [];
            $g['duration'] = $d->duration;
            $g['keystoneLevel'] = $d->keystone_level;
            $g['dungeon'] = collect($d->dungeon)->except('key');
            $g['isCompletedWithinTime'] = $d->is_completed_within_time;

            return $g;
        });

    }

    private function _getRaide($username, $server, $token){

        $res = $this->_get('https://us.api.blizzard.com/profile/wow/character/'.$server.'/'.$username.'/encounters/raids?namespace=profile-us&locale=pt_BR', $token);

        if($res == null){
            return [];
        }

        $item = json_decode($res->getBody()->getContents());

        if($item == null || !isset($item->expansions)){
            return [];
        }

        return collect($item->expansions)->filter(function ($raide){
            if($raide->expansion->id == 499) return $raide;
        })->values();

    }

    public function getServers(){

        $token = $this->_getToken();

        $client = new GuzzleHttp\Client();

        try {

            $res = $client->request('GET', 'https://us.api.blizzard.com/data/wow/realm/index?namespace=dynamic-us&locale=en_US&access_token='.$token);

        } catch (\Exception $exception){
            return null;
        }

        if($res->getStatusCode() != 200){
            return [];
        }

        $item = json_decode($res->getBody()->getContents());

        return Cache::remember('servers', 3600, function () use ($item){

            return collect($item->realms)->map(function ($realm){

                $a = [];
                $a['name'] = $realm->name;
                $a['id'] = $realm->id;
                $a['slug'] = $realm->slug;

                return $a;
            });

        });

    }

    public function _getCorredores($username, $server)
    {
        $token = $this->_getToken();

        $client = new GuzzleHttp\Client();

        try {

            $res = $this->_get('https://us.api.blizzard.com/profile/wow/character/'.$server.'/'.$username.'/achievements?namespace=profile-us&locale=pt_BR', $token);


        } catch (\Exception $exception){
            return null;
        }

        if($res->getStatusCode() != 200){
            return [];
        }

        $item = json_decode($res->getBody()->getContents());

        return collect($item->achievements)->filter(function ($achievs){

            return $achievs->id == 14468 || $achievs->id == 14469 || $achievs->id == 14470 || $achievs->id == 14471 || $achievs->id == 14472 || $achievs->id == 14568 || $achievs->id == 14569 || $achievs->id == 14569 || $achievs->id == 14570;

        })->values()->map(function ($achi){

            $a = [];
            $a['id'] = $achi->id;
            $a['name'] = $achi->achievement->name;
            $a['data'] = $achi->completed_timestamp;

            return $a;

        });


    }
}
