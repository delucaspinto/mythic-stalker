<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mythic Stalker API </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="/css/style.css" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <form class="form-signin" method="POST" action="{{ route('consulta') }}" >
            @csrf

            <div class="form-label-group">
                <select class="form-select" aria-label="Default select example" id="server" name="server">
                    <option value="" selected>Open this select menu</option>
                    @foreach($servers as $server)
                        <option value="{{ $server['slug'] }}" >{{ $server['name'] }}</option>
                    @endforeach
                </select>
            </div>

{{--            <div class="form-label-group">--}}
{{--                <input type="server" id="server" class="form-control" placeholder="Server" required autofocus>--}}
{{--                <label for="inputEmail">Server</label>--}}
{{--            </div>--}}

            <div class="form-label-group">
                <input type="character" id="character" name="character" class="form-control" placeholder="character" required>
                <label for="character">Character</label>
            </div>

            <button class="btn btn-lg btn-primary btn-block " type="submit">Search</button>
            <p class="mt-5 mb-3 text-muted text-center">&copy; 2020-2021</p>
        </form>
    </body>
</html>
