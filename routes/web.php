<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\MainController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $servers = collect(Cache::remember('servers-cache', 60, function (){
        return \App::call('App\Http\Controllers\ApiController@getServers');
    }))->sortBy('name');

    return view('welcome', compact('servers'));

});

Route::post('/consulta', [MainController::class, 'consulta'])->name('consulta');


Route::get('/character/{server}/{username}', [ApiController::class, 'getBlizzardProfile'])->name('finder');
Route::get('/servers', [ApiController::class, 'getServers']);
